package HW02;

import java.util.HashMap;
import java.util.Iterator;

public class Subject {
	public static HashMap<String, Person> team = new HashMap<String, Person>() ;
	public void registerObserver(Person person)
	{
		team.put( person.name, person);
	}
	public void deleteObserver(Person person)
	{
		team.remove(person);

	}

	public void notifyObserver()
	{
		Iterator it = team.entrySet().iterator();
		while(it.hasNext())
		{	
			( team.get(it.next()) ).action();
		}
	}
}
