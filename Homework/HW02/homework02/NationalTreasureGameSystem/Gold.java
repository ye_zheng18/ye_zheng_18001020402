package HW02;


public class Gold implements Treasure {
	public double  Quantity=500;
	private Eudemon protectors;		/**
	 * @see Treasure#guard(int, Eudemon)
	 */
	public void guard(int many) {
		protectors = new Tiger(many);
	}


	/**
	 * @see Treasure#notifyALL()
	 */
	public void notifyALL() {
		protectors.update(this.Quantity);
	}

}
