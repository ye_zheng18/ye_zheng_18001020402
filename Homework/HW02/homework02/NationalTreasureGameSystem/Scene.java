package HW02;

public class Scene {

	private Cage cage;

	private Eudemon eudemon;

	private Treasure treasurn;
	
	public void main() {
		cage = new Cage();
		Gold gold = new Gold();
		Diamond diamond = new Diamond();
		gold.guard(5);
		diamond.guard(7);
		cage.take(3, diamond);
		cage.take((132+0.5*357), gold);
	}

}
