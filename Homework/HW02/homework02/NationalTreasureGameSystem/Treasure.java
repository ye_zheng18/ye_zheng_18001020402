package HW02;


public interface Treasure {

	public abstract void guard(int many);

	public abstract void notifyALL();

}
