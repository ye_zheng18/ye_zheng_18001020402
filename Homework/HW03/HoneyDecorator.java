package HW03;

public class HoneyDecorator extends IcereamDecorator {
	public HoneyDecorator(Icecream ice) {
		// TODO Auto-generated constructor stub
		super(ice);
	}
	public String makeiceream()
	{
		return super.makeiceream()+this.addHoney();
	}
	public String addHoney()
	{
		return "+Honey";
	}
}