package HW03;

public class NuttyDecorator extends IcereamDecorator {
	public NuttyDecorator(Icecream ice) {
		// TODO Auto-generated constructor stub
		super(ice);
	}
	public String makeiceream()
	{
		return super.makeiceream()+this.addNuts();
	}
	public String addNuts()
	{
		return "+Nuts";
	}
}
