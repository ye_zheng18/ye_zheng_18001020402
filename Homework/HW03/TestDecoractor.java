package HW03;

public class TestDecoractor {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Simplelcecream s = new Simplelcecream();
		System.out.println(s.makeiceream());
		NuttyDecorator n = new NuttyDecorator(s);
		System.out.println(n.makeiceream());
		HoneyDecorator h = new HoneyDecorator(s);
		System.out.println(h.makeiceream());
	}

}
