package HW03;
public class red extends Decorate implements Color {

	public red(Room room,Curtains curtains) {
		super(room,curtains);
	}

	public String say() {
		return super.room.say()+" is red"+super.curtains.say();
	}

}
