package HW03;

public class write extends Decorate implements Color {

	public String say() {
		return super.room.say()+" is red"+super.curtains.say();
	}

	public write(Room room,Curtains curtains) {
		super(room,curtains);
	}

}
